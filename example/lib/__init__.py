#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'drogov'

import configparser

__all__ = ["Configure"]

class Configure():
    def __init__(self, config_file):
        self.config = configparser.ConfigParser()
        self.config.read(config_file)

    def read_section(self, section_name="default"):
        ret_section=""
        if self.config.has_section(section_name):
            ret_section = {}
            for key in self.config.options(section_name):
                ret_section[key] = self.config[section_name][key]
        return ret_section


    def read_param(self, param_name, default_value="", section_name="default"):
        if self.config.has_option(section_name, param_name):
            return self.config[section_name][param_name]
        else:
            return default_value