#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser

from example.lib import Configure
from hipchat_notif import *


__author__ = 'drogov'

#Parse args
parser = OptionParser()
parser.add_option("-c", "--config", dest="configfile", help="Path to config file.\nDefault: ./conf/hipchat.conf",
                  default="./my.hipchat.conf", metavar="FILE")
(options, args) = parser.parse_args()


def main():
    #Load config file
    config = Configure(options.configfile)

    # Get AUTH_TOKEN, ROOM_ID, NOTIFICATION_NAME from config
    TOKEN = config.read_param("token", section_name="hipchat")
    ROOM_ID = config.read_param("room", section_name="hipchat")
    NOTIFICATION_NAME = config.read_param("notification_name", section_name="hipchat")

    # Create a new instance
    hipchat = HipchatNotificator(TOKEN)
    hipchat.notification_name = NOTIFICATION_NAME
    hipchat.room = ROOM_ID

    #Send alert message
    hipchat.alert("Alert")
    #Send Ok message
    hipchat.ok("Not Alert")
    #Send message
    hipchat.message("Message")
    #Send Notification
    hipchat.notif("Notification")


if __name__ == '__main__':
    main()





