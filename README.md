# hipchat_notif
[![PyPI](https://img.shields.io/pypi/dd/hipchat_notif.svg?style=plastic)]()
[![PyPI](https://img.shields.io/pypi/dw/hipchat_notif.svg?style=plastic)]()
[![PyPI](https://img.shields.io/pypi/dm/hipchat_notif.svg?style=plastic)]()

[![PyPI](https://img.shields.io/pypi/v/hipchat_notif.svg?style=plastic)]()
[![PyPI](https://img.shields.io/pypi/l/hipchat_notif.svg?style=plastic)]()
[![PyPI](https://img.shields.io/pypi/pyversions/hipchat_notif.svg?style=plastic)]()
[![PyPI](https://img.shields.io/pypi/implementation/hipchat_notif.svg?style=plastic)]()
[![PyPI](https://img.shields.io/pypi/status/hipchat_notif.svg?style=plastic)]()

hipchat_notif is [HipChat](https://www.hipchat.com) client library for sending notification using API v1.

## VER. 0.2.3

Added the ability to change the message format.(Thanks to https://bitbucket.org/cou_z)
message_format: Determines how the message is treated by our server and rendered inside HipChat applications.
    html -  Message is rendered as HTML and receives no special treatment. Must be valid HTML and entities 
            must be escaped (e.g.: &amp; instead of &). May contain basic tags: a, b, i, strong, em, br, img, 
            pre, code, lists, tables. Special HipChat features such as @mentions, emoticons, and image previews 
            are NOT supported when using this format.
    text -  Message is treated just like a message sent by a user. Can include @mentions, emoticons, pastes, 
            and auto-detected URLs (Twitter, YouTube, images, etc)
## Python version

2.7
3.4

## Dependencies

- requests>=2.3

## Sample Code

```python
from hipchat_notif import *

TOKEN = "AUTH_TOKEN" # Get token from https://hipchat.com/admin/api
ROOM_ID = 10000
NOTIFICATION_NAME = "NOTIFICATOR" 

# Create a new instance.

notif =  HipchatNotificator(TOKEN)
notif.notification_name = NOTIFICATION_NAME
notif.room = ROOM_ID

# Send Alert notification 
notif.alert("Alert", message_format = "text")

# Send Ok message
notif.ok("Not Alert", message_format = "html")

# Send message
notif.message("Message")

# Send Notification
notif.notif("Notification")
```

### Correspondence table

|Method |  Notify | Color
:----: |  :----: | :----:
alert() |  True | red
ok() |  False | green
message() |  False | yellow
notif() | True | gray